//       _                   _____           _       _   
//      | |                 / ____|         (_)     | |  
//      | | __ ___   ____ _| (___   ___ _ __ _ _ __ | |_ 
//  _   | |/ _` \ \ / / _` |\___ \ / __| '__| | '_ \| __|
// | |__| | (_| |\ V / (_| |____) | (__| |  | | |_) | |_ 
//  \____/ \__,_| \_/ \__,_|_____/ \___|_|  |_| .__/ \__|
//                                            | |        
//                                            |_|


/*---------------------------------------------*\
  NOTE: 01. Fixed body when burger-menu is open
\*---------------------------------------------*/
const burgerToggler = document.querySelector('.burger__toggler');
const body = document.querySelector('body');
burgerToggler.onchange = () => body.classList.toggle('fixed');

/*-------------------------------------------------------*\
  NOTE: 02. Closes the mobile menu when a link is clicked
\*-------------------------------------------------------*/
var menuButtons = document.querySelectorAll(".menu__link")
for (i = 0; i < menuButtons.length; i++) {
  menuButtons[i].addEventListener('click', function() {
    burgerToggler.checked = false;
    body.classList.remove('fixed');
  });
}

/*-----------------------------------------------------------------------------*\
  NOTE: 03. Change CSS on Scroll
        http://www.mattmorgante.com/technology/sticky-navigation-bar-javascript
        Active script in baseof.html
\*-----------------------------------------------------------------------------*/

// Select DOM Items
const navT = document.querySelector('header');
const navB = document.querySelector('footer');
const navTop = navT.offsetTop + 80;
const navBottom = navB.offsetTop + 150;
const burgerButton = document.querySelector('.burger__button');
const menuContainer = document.querySelector('.menu-container');
const mobileStickyHeader = document.querySelector('.mobile-sticky-header');
const buttonTop = document.querySelector('.button-top');


function stickyNavigation() {

  if (window.scrollY >= navTop) {
    mobileStickyHeader.classList.add('show');
    menuContainer.classList.add('sticky');
    buttonTop.classList.add('show');
  } else {
    mobileStickyHeader.classList.remove('show');
    menuContainer.classList.remove('sticky');
    buttonTop.classList.remove('show');
  }
  if ((window.innerHeight + window.scrollY + 50) >= document.body.offsetHeight) {
    buttonTop.classList.add('bottom');
    burgerButton.classList.add('bottom');
    burgerToggler.classList.add('bottom');
  } else {
    buttonTop.classList.remove('bottom');
    burgerButton.classList.remove('bottom');
    burgerToggler.classList.remove('bottom');
  }
}

window.addEventListener('scroll', stickyNavigation);

/*------------------------------------------------------------------*\
  NOTE: 04. Vanilla JavaScript Scroll to Anchor
        https://perishablepress.com/vanilla-javascript-scroll-anchor
        Add class 'scroll' to HTML-Element to activate
\*------------------------------------------------------------------*/

(function() {
	scrollTo();
})();

function scrollTo() {
	const links = document.querySelectorAll('.scroll');
	links.forEach(each => (each.onclick = scrollAnchors));
}

function scrollAnchors(e, respond = null) {
	const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);
	e.preventDefault();
	var targetID = (respond) ? respond.getAttribute('href') : this.getAttribute('href');
	const targetAnchor = document.querySelector(targetID);
	if (!targetAnchor) return;
	const originalTop = distanceToTop(targetAnchor);
	window.scrollBy({ top: originalTop, left: 0, behavior: 'smooth' });
	const checkIfDone = setInterval(function() {
		const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
		if (distanceToTop(targetAnchor) === 0 || atBottom) {
			targetAnchor.tabIndex = '-1';
			targetAnchor.focus();
			window.history.pushState('', '', targetID);
			clearInterval(checkIfDone);
		}
	}, 100);
}

/*-----------------------------------------------*\
  NOTE: 05. Tiny-Slider
        https://github.com/ganlanyuan/tiny-slider
        Activate script in baseof.html
\*-----------------------------------------------*/
/*
var slider = tns({
  container: '#slider .slides',
  autoplay: false,
  autoplayButton: false,
  autoplayButtonOutput: false,
  controlsContainer: '#slider .controls'
});

/* For Multiple Tiny-Slider */
/*
i = 0;
$('.tinyslider').each(function() {
  ++i;
  var slider = tns({
    container: '#slider'+i,
    items: 1,
    autoplay: true,
    nav: false,
    controlsContainer: '#controls'+i,
    autoplayButtonOutput: false
  });
});
*/