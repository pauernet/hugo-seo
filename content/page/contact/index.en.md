---
##################################################
# These fields should be completed for each file #
##################################################
title: "Give us a call!" #Max 70 characters
slug: "contact" #the content slug url
description: "We're looking forward to get in contact with you." #Max 160 characters
keywords: contact,address,phone,call #Ignored by Google
#tags: ["tag1","tag2","tag3"] #For Taxonomy Templates
#categories: ["cat1"] #For Taxonomy Templates
draft: false #or true to not render
#language: "en" #Identification for NetlifyCMS
type: "page" #Template folder in /layouts/
layout: "contact" #Template file 

############################################
# There are different types of schema      #
# e.g. WebPage, Article, Book, Recipe...   #
# Some of them are already created in the  #
# file /layouts/partials/meta_schema.html. #
# Info: https://schema.org/docs/full.html  #
############################################
schemaType: "" #Default: LocalBusiness

#################################
# If these fields are empty     #
# the information will be taken #
# from /data/siteVar.yaml       #
#################################
publisher: "" #Agency or company
copyright: "" #Your name or company
author: "" #Person who wrote this text/article
pageType: #Type of the page e.g. description, help
  - contact
  - phone
  - address
  - call
pageTopic: #Categories e.g. business, science, ...
  - informationen
audience: "" #Target group e.g. designer, all
publishDate: "" #First release of this page e.g. 2020-01-19T15:01:05+01:00
lastModDate: "" #Last modification date of this page e.g. 2020-01-21T15:01:05+01:00
robots: "" #index, follow, none...
revisitAfter: "" #Bot revisit page e.g. 14 days
openGraphImage: "" #1200x630px #Located in same directory #Use File-Extension .jpg or .png
---
<!-- Info comes from siteVar.yaml -->