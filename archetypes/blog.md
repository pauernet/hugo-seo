---
################################################
# These fields must be completed for each file #
################################################
title: "{{ replace .Name "-" " " | title }}" #Max 70 characters
slug: "{{ replace .Name "-" " " | title }}" #the content slug url
description: "Das ist ein Blogartikel" #Max 160 characters
keywords: artikel1, deutsch #Ignored by Google
tags: ["tag1","tag2","tag3"] #For Taxonomy Templates
categories: ["cat1"] #For Taxonomy Templates
draft: false #or true to not render
#language: "de" #Identification for NetlifyCMS
type: "blog" #Template folder in /layouts/
layout: "single" #Template file 

############################################
# There are different types of schema      #
# e.g. WebPage, Article, Book, Recipe...   #
# Some of them are already created in the  #
# file /layouts/partials/meta_schema.html. #
# Info: https://schema.org/docs/full.html  #
############################################
schemaType: "Article" #Default: LocalBusiness

#################################
# If these fields are empty     #
# the information will be taken #
# from /data/siteVar.yaml       #
#################################
publisher: "" #Agency or company
copyright: "" #Your name or company
author: "" #Person who wrote this text/article
pageType: #Type of the page e.g. description, help
  - news
  - blog
  - article
pageTopic: #Categories e.g. business, science, ...
#  - topic2
#  - topic1
audience: "" #Target group e.g. designer, all
publishDate: 2018-08-16T12:51:39 #e.g. 2020-01-19T15:01:05
lastModDate: 2018-08-17T15:01:05 #e.g. 2020-01-21T15:01:05
robots: "" #index, follow, none...
revisitAfter: "" #Bot revisit page e.g. 14 days
openGraphImage: "" #1200x630px #Located in same directory #Use File-Extension .jpg or .png
---
Hallo das ist Artikel 1 von letztem Jahr!