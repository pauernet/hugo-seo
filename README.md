# HUGO SEO
This template was developed especially for SEO and multilingual websites. It's still in development.

## Getting Started with your own Static Site

### Prerequisites
- Install HUGO extended [Installation guide](https://gohugo.io/getting-started/installing/)
- Install Git [Installation guide](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
 
### SFTP-Variables (Hosting on your own FTP-Server)
- Go to GitLab.com > YOUR_NEW_PROJECT > Settings > CI-CD > Variables
- Add the Keys (Variable): SFTP_FOLDER, SFTP_HOST, SFTP_USERNAME, SFTP_PASSWORT 
- Enter your FTP-Data

### Icons, Logos and OpenGraph-Image
- Create a SVG-Icon (260x260px)
- Go to https://realfavicongenerator.net and upload the SVG-Icon
- Choose Path "/images/icons/favicons"
- Download the Facicon package and extract the content to "/static/images/icons/favicons"
- Check the HTML-Tags in "/layouts/partials/favicons.html"
- Replace logo.svg in "/static/images/icons/logo.svg" with your own logo
- Replace logo.png in "/static/images/schema_opengraph/logo.png" with your own logo
- Replace the OpenGraph-Image in "/static/images/schema_opengraph/opengraph.jpg" with your own image
- apple-touch-icon.png & favicon.png has to be in the root directory "/static/"

### Additional Informationen
- Change informations in "/data/siteVar.yaml"
- Take a look in every "/content/*.md" file for additional informations

## hugo Commands
1. Start server
`hugo-server`
2. Build site
`hugo`

## Deploy website on your own FTP-Server
- Visit GitLab.com
- Choose your Project
- CI / CD
- Pipelines
- Click the second hook
- Click Play-Button at deploy_pages

## Deploy Website on Netlify
- Visit Netlify.com
- Register and connect to your GitLab Repository
- Follow instructions for connecting your own Domain with SSL

## Mentions

### 7-1 SASS-Boilerplate
`https://github.com/HugoGiraudel/sass-boilerplate`

### BEM - Block Element Modifier
`http://getbem.com`

### ASCII-Generator
`http://www.network-science.de/ascii (Fonts: big + small)`

### SASS Mixins
```
http://zerosixthree.se/8-sass-mixins-you-must-have-in-your-toolbox
https://engageinteractive.co.uk/blog/top-10-scss-mixins
```

### Real Favicon Generator
`https://realfavicongenerator.net`

### CSS filter generator
`https://codepen.io/sosuke/pen/Pjoqqp`

### HTML/CSS Menu
`https://codepen.io/bradtraversy/pen/vMGBjQ`

### Tiny Slider
`https://github.com/ganlanyuan/tiny-slider`

### Sticky Navigation Bar
`http://www.mattmorgante.com/technology/sticky-navigation-bar-javascript`

### Scroll to Anchor
`https://perishablepress.com/vanilla-javascript-scroll-anchor/`

### htaccess file from Andreas Hecht
`https://gist.github.com/HechtMediaArts/c96bc796764baaa64d43b70731013f8a`